def codeCheckout(module, scmUrl) {

checkout([
$class: 'GitSCM', 
branches: [[name: '*/$Brach_Name']], 
doGenerateSubmoduleConfigurations: false, 
extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: module]],
submoduleCfg: [], 
userRemoteConfigs: [[url: scmUrl]]
])

}